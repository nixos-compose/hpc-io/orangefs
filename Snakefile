PATH_HERE="/home/quentin/ghq/gitlab.inria.fr/nixos-compose/hpc-io/orangefs"
RESULT_FOLDER=f"{PATH_HERE}/data"


FLAVOURS = [
    "g5k-image"
]

NB_CPU_NODES = [
    8, 16, 24, 32
]

NB_IO_NODES = [
    1, 2, 4
]


BLOCK_SIZES = [
    "1M",
    "10M",
    "100M"
    # "1G",
]

FIGS = [
    "ratio_folding.pdf"
]


EXPES_RESULTS = expand(["{path_here}/data/json_zip/results_ior_{nb_cpu_nodes}_cpu_nodes_{nb_io_nodes}_io_nodes_{block_size}_block_size_{flavour}.zip"], path_here=PATH_HERE,
                                                                                                                                                        flavour=FLAVOURS,
                                                                                                                                                        nb_cpu_nodes=NB_CPU_NODES,
                                                                                                                                                        nb_io_nodes=NB_IO_NODES,
                                                                                                                                                        block_size=BLOCK_SIZES)



rule all:
    input:
        EXPES_RESULTS,
        expand(["figs/{name}"], name=FIGS)


rule build_nxc_image:
    input:
        "nxc/flake.nix",
        "nxc/flake.lock",
        "nxc/composition.nix",
        "nxc/my_scripts.nix",
        "nxc/script_ior.nix"
    output:
        "nxc/build/composition::{flavour}"
    shell:
        "cd nxc; nix develop --command nxc build -f {wildcards.flavour}"

rule run_ior:
    input:
        "nxc/build/composition::{flavour}"
    output:
        "{PATH_HERE}/data/json_zip/results_ior_{nb_cpu_nodes}_cpu_nodes_{nb_io_nodes}_io_nodes_{block_size}_block_size_{flavour}.zip"
    shell:
        "cd nxc; nix develop --command python3 script.py --nxc_build_file {PATH_HERE}/{input} --nb_cpu_nodes {wildcards.nb_cpu_nodes} --nb_io_nodes {wildcards.nb_io_nodes} --block_size {wildcards.block_size} --result_dir {RESULT_FOLDER} --flavour {wildcards.flavour} --outfile {output} --walltime 5"


rule json_to_csv:
    input:
        "analysis/ior_json_to_csv.py",
        "{PATH_HERE}/data/json_zip/results_ior_{nb_cpu_nodes}_cpu_nodes_{nb_io_nodes}_io_nodes_{block_size}_block_size_{flavour}.zip"
    output:
        "{PATH_HERE}/data/csv_zip/results_csv_ior_{nb_cpu_nodes}_cpu_nodes_{nb_io_nodes}_io_nodes_{block_size}_block_size_{flavour}.zip"
    shell:
        "nix develop .#python --command python3 {input} {output}"

rule generate_csv:
    input:
        "{PATH_HERE}/analysis/zip_to_csv.R",
        "{PATH_HERE}/data/csv_zip/results_csv_ior_{nb_cpu_nodes}_cpu_nodes_{nb_io_nodes}_io_nodes_{block_size}_block_size_{flavour}.zip"
    output:
        "{PATH_HERE}/data/csv/{nb_cpu_nodes}_{nb_io_nodes}_{block_size}_{flavour}.csv"
    shell:
        "nix develop .#rshell --command Rscript {input} {output} {wildcards.nb_cpu_nodes} {wildcards.nb_io_nodes} {wildcards.block_size}"

rule generate_plots:
    input:
        "analysis/plot_{plot_name}.R",
        expand(["{path_here}/data/csv/{nb_cpu_nodes}_{nb_io_nodes}_{block_size}_{flavour}.csv"], path_here=PATH_HERE, flavour=FLAVOURS, nb_cpu_nodes=NB_CPU_NODES, nb_io_nodes=NB_IO_NODES, block_size=BLOCK_SIZES),
    output:
        "figs/{plot_name}.pdf"
    shell:
        "nix develop .#rshell --command Rscript {input} {output}"
