{ pkgs, setup, ... }: {

  nodes =
    let
      scripts = import ./my_scripts.nix { inherit pkgs setup; };
      common_config = { pkgs, ... }:
        {
          environment.etc = {
            ior_script = {
              text = import ./script_ior.nix { inherit pkgs setup; };
            };
          };
          networking.firewall.enable = false;
        };
    in
    {

      serverfs = { pkgs, ... }: {
        imports = [ common_config ./module.nix ];
        environment.systemPackages = with scripts; [
          gen_config_orangefs
          start_orangefs
          pkgs.orangefs
        ];

        systemd.tmpfiles.rules =
          [
            "d /data 0770 orangefs orangefs"
            "d /data/storage 0770 orangefs orangefs"
            "d /data/meta 0770 orangefs orangefs"
          ];

        users.users.orangefs = {
          isSystemUser = true;
          group = "orangfs";
        };
        
        users.groups.orangefs = { };

      };

      client = { lib, ... }: {
        imports = [ common_config ];

        environment.systemPackages = with pkgs; [
          ior
          openmpi
          orangefs
        ] ++ (with scripts; [
          start_ior_nodes
          generate_ior_config
          orangefs_mount
        ]);

        boot.supportedFilesystems = [ "pvfs2" ];
        boot.kernelModules = [ "orangefs" ];
      };

    };
  testScript = ''
  '';
}
