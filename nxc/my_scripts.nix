{ pkgs, setup, ... }:
let
  # We define some constants here
  nfsMountPoint = setup.params.nfsMountPoint;
  numTasks = setup.params.numTasks;
  iorConfig = "/etc/ior_script";

  iorConfigPerCluster = "${nfsMountPoint}/ior_script";
in {
  # This function takes the number of compute nodes,
  # creates a hostfile for MPI and runs the benchmark
  start_ior =
    pkgs.writeScriptBin "start_ior" ''
      cd ${nfsMountPoint}

      NB_NODES=$(cat /etc/hosts | grep node | wc -l)
      NB_SLOTS_PER_NODE=$((${builtins.toString numTasks} / $NB_NODES))

      cat /etc/hosts | grep node | awk -v nb_slots="$NB_SLOTS_PER_NODE" '{ print $2 " slots=" nb_slots;}' > my_hosts

      mpirun --allow-run-as-root --oversubscribe -mca btl self,vader -np ${builtins.toString numTasks} --hostfile my_hosts ior -f ${iorConfig}
    '';

  generate_ior_config =
    pkgs.writeScriptBin "generate_ior_config" ''
      cd ${nfsMountPoint}
      cp ${iorConfig} ${iorConfigPerCluster}
      NB_TASKS=$1
      BLOCK_SIZE=$2

      sed -ri "s/(numTasks)=\w+/\1=$NB_TASKS/g" ${iorConfigPerCluster}
      sed -ri "s/(blockSize)=\w+/\1=$BLOCK_SIZE/g" ${iorConfigPerCluster}
  '';

  start_ior_nodes =
    pkgs.writeScriptBin "start_ior_nodes" ''
      cd ${nfsMountPoint}

      NB_NODES=$1
      TOTAL_NB_NODES=$2

      NB_SLOTS_PER_NODE=$(($TOTAL_NB_NODES / $NB_NODES))

      cat /etc/hosts | grep client | head -n $NB_NODES | awk -v nb_slots="$NB_SLOTS_PER_NODE" '{ print $2 " slots=" nb_slots;}' > my_hosts

      # mpirun --allow-run-as-root --oversubscribe -mca btl self,vader -np $TOTAL_NB_NODES --hostfile my_hosts ior -f ${iorConfigPerCluster}
      mpirun --allow-run-as-root --oversubscribe --mca pml ^ucx --mca mtl ^psm2,ofi --mca btl ^ofi,openib -np $TOTAL_NB_NODES --hostfile my_hosts ior -f ${iorConfigPerCluster}
    '';
    
  gen_config_orangefs = 
    pkgs.writeScriptBin "gen_config_orangefs" ''
      ${pkgs.python3}/bin/python3 ${./gen_config_orangefs.py} $@
      # SERVERS=$1
      # ${pkgs.orangefs}/bin/pvfs2-genconfig --quiet --ioservers $SERVERS --protocol tcp --port 3334 --metaservers $SERVERS --storage /data --logfile /var/log/orangefs-server.log -
    '';
    
  start_orangefs = 
    pkgs.writeScriptBin "start_orangefs" ''
        CONFIG=/tmp/pvfs.conf
        ${pkgs.python3}/bin/python3 ${./gen_config_orangefs.py} $@ > $CONFIG
        ${pkgs.orangefs}/bin/pvfs2-server -f $CONFIG
        ${pkgs.orangefs}/bin/pvfs2-server $CONFIG
    '';
    
  orangefs_mount = 
    pkgs.writeScriptBin "orangefs_mount" ''
      SERVER=$1
      ${pkgs.orangefs}/bin/pvfs2-client
      mkdir /orangefs
      sleep 5
      mount -t pvfs2 tcp://$SERVER:3334/orangefs /orangefs
    '';
}
