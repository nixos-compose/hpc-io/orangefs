import sys

MAX_HANDLE = 9223372036854775806 # 2^63

def generate_config(aliases, max_handle=MAX_HANDLE):
    handle_step = (max_handle / len(aliases)) / 2
    
    alias_list = "\n".join(f"Alias {name} tcp://{name}:3334" for name in aliases)
    
    meta_ranges = "\n".join(f"Range {name} {int(i * handle_step + 3)}-{int(i * handle_step + 3 + handle_step - 1)}" for (i, name) in enumerate(aliases))
    data_ranges = "\n".join(f"Range {name} {int(i * handle_step + 3 + len(aliases) * handle_step)}-{int(i * handle_step + 3 + len(aliases) * handle_step + handle_step - 1)}" for (i, name) in enumerate(aliases))
    
    config = f"""
<Defaults>
LogType syslog
DataStorageSpace /data/storage
MetaDataStorageSpace /data/meta
BMIModules bmi_tcp
</Defaults>
<Aliases>
{alias_list}
</Aliases>
<FileSystem>
Name orangefs
ID 1
RootHandle 3

<MetaHandleRanges>
{meta_ranges}
</MetaHandleRanges>

<DataHandleRanges>
{data_ranges}
</DataHandleRanges>

<StorageHints>
TroveSyncMeta yes
TroveSyncData no
</StorageHints>
</FileSystem>
    """
    return config
    
def main():
    args = sys.argv
    aliases = args[1:]
    print(generate_config(aliases))
    return
    
main()
