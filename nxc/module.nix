{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.my-startup;
in
{

  ###### interface


  options = {
    services.my-startup = {
      enable = mkEnableOption "My startup ";

      package = mkOption {
      type = types.package;
      default = pkgs.orangefs;
      defaultText = " pkgs.orangefs";
    };
    };
  };

  ###### implementation

  config = mkIf (cfg.enable) {
    systemd.services.my-startup = {
      description = "My startup";
      wantedBy = [ "orangefs-server.service" ];
      after = [ "network.target" ];
      serviceConfig.Type = "oneshot";
      script = ''
        ${pkgs.sudo}/bin/sudo -u orangefs -g orangefs ${cfg.package}/bin/pvfs2-server /etc/orangefs/server.conf -f
      '';
    };
  };
}
